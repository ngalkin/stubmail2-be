'use strict';

/* eslint-env es6 */
let async   = require('asyncawait/async');
let await   = require('asyncawait/await');
let mailin  = require('mailin');
let _       = require('lodash');
let app     = require('./app');
let User    = app.db.model('User');
let Project = app.db.model('Project');
let Email   = app.db.model('Email');


mailin.start({
  port: app.config.smtp.port || 2520,
  host: app.config.smtp.host,
  disableWebhook: true,
  disableDnsLookup: true,
  smtpOptions: {
    SMTPBanner: 'StubMail SMTP Server',
    secure: false,
    disabledCommands: ['AUTH']
  }
});

mailin.on('message', async.cps((connection, data, source) => {
  let email = _.pick(data, ['from', 'to', 'cc', 'subject', 'html', 'text', 'headers', 'messageId', 'date', 'attachments']);
  email.source = source;

  let project = email._project = await(Project.findOne({title: email.headers['x-project']}).exec());
  let user    = email._user    = await(User.findOne({login: email.headers['x-user']}).exec());

  if (~[user, project].indexOf(null)) {
    console.log('Empty');
    // @todo: need logging
    return;
  }

  await(Email.saveEmail.bind(Email, email));
}));

FROM node:4.2

MAINTAINER Nikolay Galkin

WORKDIR /src
COPY . /src

RUN npm install

EXPOSE 2510 2520

'use strict';

/**
 * @param {Application} app
 * @returns {Generator}
 */
module.exports = app => {
  require('json5/lib/require');
  let  _     = require('lodash');
  app.config = require('../config/default.json5');

  _.extend(app.config, {permissions: require('../config/permissions.json5')}); // permissions
  try {
    _.extend(app.config, require(`../config/${app.env}.json5`));
  } catch (e) {}

  return function*(next) {
    this.config = app.config;
    yield next;
  };
};

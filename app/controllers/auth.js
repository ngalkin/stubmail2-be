'use strict';

let LdapAuth = require('ldapauth-fork');
let router   = require('koa-router')({prefix: '/auth'});
let app      = require('../../app');
let User     = app.db.model('User');

module.exports = router.routes();

router
  .post('/login', app.acl.can('auth:login'), function*() {
    this.checkBody('login').notEmpty().trim().toLow();
    this.checkBody('password').notEmpty().len(6, 20).trim();
    if (this.errors) {
      this.throw(422, 'Invalid login or password');
    }

    // @todo: do it better
    let auth = new LdapAuth(app.config.auth.ldap);
    let user = yield auth.authenticate.bind(auth, this.request.body.login, this.request.body.password);
    yield auth.close.bind(auth);
    user = {
      login: this.request.body.login,
      email: user.mail,
      firstName: user.givenName,
      lastName: user.sn,
      department: user.department
    };
    user = yield User.sync(user);
    let token = this.jwt.sign(user, this.config.auth.jwt.secret, {expiresInMinutes: 60 * 5});
    this.body = {token: token};
  })

  .post('/register', app.acl.can('auth:register'), function*() { // Duplicate of POST /users
    let user = new User(this.request.body);
    yield user.save();
    this.body = user;
    this.status = 201;
  })

  .post('/logout', app.acl.can('auth:logout'), function*() {
    delete this.state.user;
    this.status = 201;
  })

  .get('/profile', app.acl.can('auth:profile'), function*() {
    this.body = this.state.user;
  });

'use strict';

let Resource = require('koa-resource-router');
let _        = require('lodash');
let app      = require('../../app');
let User     = app.db.model('User');

module.exports = new Resource('users', {
  index: [app.acl.can('users:list'), function*() {
    let filter = this.request.query && this.request.query.filter;
    let query = User.find();
    if (filter && filter.login && filter.login.like) {
      query.where('login').equals(new RegExp(filter.login.like));
    }
    this.body = yield query.select('-password').exec();
  }],

  create: [app.acl.can('users:create'), function*() {
    this.checkBody('login').notEmpty().trim().toLow();
    this.checkBody('email').notEmpty().isEmail('Invalid Email').trim().toLow();
    this.checkBody('password').notEmpty().len(6, 20).trim();
    this.checkBody('firstName').notEmpty().isAlpha().trim();
    this.checkBody('lastName').notEmpty().isAlpha().trim();
    if (this.errors) {
      this.throw(422, 'Invalid params');
    }

    let params = _.pick(this.request.body, User.FIELDS);
    let user = new User(params);
    yield user.save();
    this.body = user;
    this.status = 201;
  }],

  show: [app.acl.can('users:show'), userParam, function*() {
    this.body = this.item;
  }],

  update: [app.acl.can('users:update'), userParam, function*() {
    //this.checkBody('firstName').notEmpty().isAlpha().trim();
    //this.checkBody('lastName').notEmpty().isAlpha().trim();
    this.checkBody('role').notEmpty().isAlpha().trim();
    if (this.errors) {
      this.throw(422, 'Invalid params');
    }

    let params = _.pick(this.request.body, ['firstName', 'lastName', 'role', '_users']);
    yield this.item.set(params).save();
    this.body = this.item;
  }],

  destroy: [app.acl.can('users:destroy'), userParam, function*() {
    yield this.item.remove();
    this.status = 204;
  }]
}).middleware();


function *userParam(next) {
  this.params.user !== 'me' && this.checkParams('user').isObjectId();
  if (this.errors) {
    this.throw(422, 'Invalid User id');
  }

  let id = this.params.user === 'me' ? this.state.user._id : this.params.user;
  this.item = yield User.findById(id);
  if (!this.item) {
    this.throw(404, 'User not found');
  }
  yield next;
}

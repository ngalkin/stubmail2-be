'use strict';

let Resource = require('koa-resource-router');
let _        = require('lodash');
let app      = require('../../app');
let Email    = app.db.model('Email');
let Project  = app.db.model('Project');

module.exports = new Resource('emails', {
  index: [app.acl.can('emails:list'), function*() {
    let projects = yield Project.find({_users: this.state.user._id}).exec();
    let filter = _.extend({_project: {$in: projects}}, this.request.query && this.request.query.filter);
    this.body = yield Email.find(filter, {__v: false}).populate('_project _user').sort('-created').exec();
  }],

  show: [app.acl.can('emails:show'), emailParam, function*() {
    yield this.item.set({isRead: true}).save();
    this.body = this.item;
  }],

  update: [app.acl.can('emails:update'), emailParam, function*() {
    if (this.errors) {
      this.throw(422, 'Invalid params');
    }

    let params = _.pick(this.request.body, Email.FIELDS);
    yield this.item.set(params).save();
    this.body = this.item;
  }],

  destroy: [app.acl.can('emails:destroy'), emailParam, function*() {
    yield this.item.remove();
    this.status = 204;
  }]
}).middleware();


function *emailParam(next) {
  this.checkParams('email').isObjectId();
  if (this.errors) {
    this.throw(422, 'Invalid Email id');
  }

  this.item = yield Email.findById(this.params.email);
  if (!this.item) {
    this.throw(404, 'Email not found');
  }
  yield next;
}

'use strict';

let mongoose        = require('mongoose');
let timestamps      = require('mongoose-timestamp');
let uniqueValidator = require('mongoose-unique-validator');
let randomColor     = require('randomcolor');

const FIELDS = ['_id', '_users', 'title', 'color'];

let schema = new mongoose.Schema({
  title: {type: String, trim: true, required: true},
  color: {type: String, trim: true, default: randomColor},
  _users: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});

schema.plugin(timestamps, {createdAt: 'created', updatedAt: 'updated'});
schema.plugin(uniqueValidator, 'Error, expected {PATH} to be unique');

schema.statics.FIELDS = FIELDS;

mongoose.model('Project', schema);

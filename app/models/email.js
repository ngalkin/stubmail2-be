'use strict';

let async    = require('asyncawait/async');
let await    = require('asyncawait/await');
let mongoose = require('mongoose');
let _        = require('lodash');
let path     = require('path');
let fs       = require('fs');
let Schema   = mongoose.Schema;

const FIELDS = ['_id', 'title', 'isDeleted', 'isRead'];

let addressSchema = new Schema({
  name:    {type: String, trim: true},
  address: {type: String, trim: true}
});

let attachmentSchema = new Schema({
  contentType:        String,
  contentId:          String,
  contentDisposition: String,
  fileName:           String,
  transferEncoding:   String,
  generatedFileName:  String,
  checksum:           String,
  length:             Number
});

let emailSchema = new mongoose.Schema({
  _user:       {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  _project:    {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
  from:        [addressSchema],
  to:          [addressSchema],
  cc:          [addressSchema],
  subject:     {type: String, trim: true},
  html:        {type: String, trim: true},
  text:        {type: String, trim: true},
  headers:     Object,
  messageId:   String,
  date:        Date,
  attachments: [attachmentSchema],
  source:      {type: String, trim: true},
  isRead:      {type: Boolean, default: false},
  isDeleted:   {type: Boolean, default: false}
});

emailSchema.index({isDeleted: 1});

emailSchema.plugin(require('mongoose-timestamp'), {createdAt: 'created', updatedAt: 'updated'});
emailSchema.plugin(require('mongoose-unique-validator'), 'Error, expected {PATH} to be unique');

emailSchema.statics.FIELDS = FIELDS;

emailSchema.statics.saveEmail = async.cps(function(data) {
  let attachments = _.clone(data.attachments);
  let folder = './build/attachments';

  // Save email
  let email = new this(data);
  await(email.save.bind(email));

  // Save attachments
  folder = path.join(folder, ''+email._id);
  fs.mkdirSync(folder);

  attachments.forEach(function(attachment) {
    fs.writeFileSync(path.join(folder, attachment.generatedFileName), attachment.content);
    email.html && (email.html = email.html.replace(new RegExp('cid:([^><"]+)'), 'attachments/' + email._id + '/' + attachment.generatedFileName));
  });
  return await(email.save.bind(email));
});

mongoose.model('Email', emailSchema);

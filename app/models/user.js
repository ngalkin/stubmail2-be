'use strict';

let mongoose        = require('mongoose');
let timestamps      = require('mongoose-timestamp');
let uniqueValidator = require('mongoose-unique-validator');
let async           = require('asyncawait/async');
let await           = require('asyncawait/await');
let _               = require('lodash');

const ROLE_GUEST = 'guest';
const ROLE_USER  = 'user';
const ROLE_ADMIN = 'admin';
const ROLES      = [ROLE_GUEST, ROLE_USER, ROLE_ADMIN];
const FIELDS     = ['_id', 'login', 'email', 'firstName', 'lastName', 'role'];

let schema = new mongoose.Schema({
  login:       {type: String,  trim: true, required: true, unique: true},
  email:       {type: String,  trim: true, required: true, unique: true},
  role:        {type: String, trim: true, required: true, default: ROLE_USER, enum: ROLES},
  firstName:   {type: String,  trim: true, required: true},
  lastName:    {type: String,  trim: true, required: true}
});

schema.plugin(timestamps, {createdAt: 'created', updatedAt: 'updated'});
schema.plugin(uniqueValidator, 'Error, expected {PATH} to be unique');

schema.statics.FIELDS = FIELDS;
schema.statics.ROLE_GUEST = ROLE_GUEST;
schema.statics.ROLE_USER = ROLE_USER;
schema.statics.ROLE_ADMIN = ROLE_ADMIN;

schema.statics.sync = async(function(data) {
  let params = _.pick(data, FIELDS);
  let user = await(this.findOne({login: params.login}).select(FIELDS.join('+ ')).exec());
  if (!user) {
    user = new this(params);
    await(user.save.bind(user));
  }
  return _.pick(user._doc, FIELDS);
});

mongoose.model('User', schema);
